<?php

require_once 'vendor/autoload.php';
require_once './librerias/fpdf181/fpdf.php';

$app = new \Slim\Slim();

// Configuracion de cabeceras para permitir peticiones AJAX.
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}

// Archivos externos
require_once './Modulo.php';

require_once './controladores/empleado.controlador.php';
require_once './modelos/empleado.modelo.php';

require_once './controladores/movimiento.controlador.php';
require_once './modelos/movimiento.modelo.php';

require_once './controladores/nomina.controlador.php';
require_once './modelos/nomina.modelo.php';

function mensajeError($ex) {
    $response = array(
        'status' => 'error',
        'extra' => utf8_encode($ex->getMessage()),
        'code' => 500,
        'data' => 'Verificar backend ó conexión con la base de datos.'
    );
    return json_encode($response);
}

function devolver($data) {
    $response = array(
        'status' => 'success',
        'code' => 200,
        'data' => $data
    );
    return json_encode($response);
}

$app->run();
