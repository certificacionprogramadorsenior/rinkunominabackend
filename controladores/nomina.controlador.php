<?php

#region Nomina
function crearNominaC($mes, $ano) {
    $response = null;
    try {
        $response = crearNominaM($mes, $ano);
        crearPDFC($mes, $ano);
        echo devolver($response);
    } catch(\Exception $ex) {
        echo mensajeError($ex);
    }
}

function crearPDFC($mes, $ano) {
    $response = null;

    $response = pedirInformacion($mes, $ano);
    $nombre = 'nomina-'.$mes.'-'.$ano.'.pdf';

    $pdf = new FPDF();
    $pdf->SetFont('Helvetica', 'B', 12);
    foreach ($response as $empleado) {
        $pdf->AddPage();
        $pdf->Write (7,"Nomima RINKU");
        $pdf->Ln(15);
        $pdf->Write (7,utf8_decode("Número de empleado: "));
        $pdf->Write (7,$empleado->clave_empleado);
        $pdf->Ln(7);
        $pdf->Write (7,utf8_decode("Nombre de empleado: "));
        $pdf->Write (7,utf8_decode($empleado->nombre));
        $pdf->Ln(7);
        $pdf->Write (7,utf8_decode("Rol: "));
        $pdf->Write (7,utf8_decode($empleado->rol));
        $pdf->Ln(7);
        $pdf->Write (7,utf8_decode("Tipo: "));
        $pdf->Write (7,utf8_decode($empleado->tipo));
        $pdf->Ln(7);

        //Calculos
        $SueldoTotal = $empleado->SueldoBase + $empleado->SueldoEntrega + $empleado->SueldoBono;
        $ISR = $SueldoTotal * .09;
        if ($SueldoTotal > 16000) {
            $ISRAdicional = $SueldoTotal * .03;
        } else {
            $ISRAdicional = 0;
        }
        if ($empleado->tipo != "EXTERNO") {
            $ValeDespensa = $SueldoTotal * .04;
        } else {
            $ValeDespensa = 0;
        }
        $TotalIngreso = $SueldoTotal + $ValeDespensa;
        $TotalEgreso = $ISR + $ISRAdicional;
        $TotalAPagar = $TotalIngreso - $TotalEgreso;

        $pdf->Multicell(190,7,
            "                                             " . "Ingresos"   . "                   " . "Egresos\n" .
            "SUELDO                              " . $SueldoTotal . "\n" .
            "VALE DE DESPENSA         " . $ValeDespensa . "\n" .
            "-ISR                                                                        " . $ISR . "\n" .
            "-ISR ADICIONAL                                                   " . $ISRAdicional . "\n" .
            "\n" .
            "TOTALES                             " . $TotalIngreso . "                        " . $TotalEgreso . "\n" .
            "\n" .
            "A PAGAR                             " . $TotalAPagar
        ,1);

    }
    
    $pdf->Output("./archivosPDF/". $nombre,'F');
}

#endregion Nomina
