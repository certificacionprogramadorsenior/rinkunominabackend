<?php

#region Empleados
    function crearEmpleadoC($datos) {
        $response = null;
        try {
            $response = crearEmpleadoM($datos);
            echo devolver($response);
        } catch(\Exception $ex) {
            echo mensajeError($ex);
        }
    }

    function consultarEmpleadosC() {
        $response = null;
        try {
            $response = consultarEmpleadosM();
            echo devolver($response);
        } catch(\Exception $ex) {
            echo mensajeError($ex);
        }
    }

    function consultarEmpleadoC($id) {
        $response = null;
        try {
            $response = consultarEmpleadoM($id);
            echo devolver($response);
        } catch(\Exception $ex) {
            echo mensajeError($ex);
        }
    }

    function actualizaEmpleadoC($datos) {
        $response = null;
        try {
            $response = actualizaEmpleadoM($datos);
            echo devolver($response);
        } catch(\Exception $ex) {
            echo mensajeError($ex);
        }
    }

    function eliminaEmpleadoC($id) {
        $response = null;
        try {
            $response = eliminaEmpleadoM($id);
            echo devolver($response);
        } catch(\Exception $ex) {
            echo mensajeError($ex);
        }
    }
#endregion Empleados
