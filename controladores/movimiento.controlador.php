<?php

#region Movimientos
function crearMovimientoC($datos) {
    $response = null;
    try {
        $response = crearMovimientoM($datos);
        echo devolver($response);
    } catch(\Exception $ex) {
        echo mensajeError($ex);
    }
}

function consultarMovimientosC() {
    $response = null;
    try {
        $response = consultarMovimientosM();
        echo devolver($response);
    } catch(\Exception $ex) {
        echo mensajeError($ex);
    }
}

function consultarMovimientoC($id) {
    $response = null;
    try {
        $response = consultarMovimientoM($id);
        echo devolver($response);
    } catch(\Exception $ex) {
        echo mensajeError($ex);
    }
}

function actualizaMovimientoC($datos) {
    $response = null;
    try {
        $response = actualizaMovimientoM($datos);
        echo devolver($response);
    } catch(\Exception $ex) {
        echo mensajeError($ex);
    }
}

function eliminaMovimientoC($id) {
    $response = null;
    try {
        $response = eliminaMovimientoM($id);
        echo devolver($response);
    } catch(\Exception $ex) {
        echo mensajeError($ex);
    }
}

function fechaMovimientoC() {
    $response = null;
    try {
        $response = fechaMovimientoM();
        echo devolver($response);
    } catch(\Exception $ex) {
        echo mensajeError($ex);
    }
}

function consultarEmpleadoMovC($id) {
    $response = null;
    try {
        $response = consultarEmpleadoMovM($id);
        echo devolver($response);
    } catch(\Exception $ex) {
        echo mensajeError($ex);
    }
}
#endregion Movimientos
