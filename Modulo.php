<?php

// Peticiones
#region Empleados
$app->post('/crear/empleado', function() use ($app) {
    $json = $app->request->post('json');
    $datos = json_decode($json, true);
    crearEmpleadoC($datos);
});

$app->get('/consultar/empleados', function() use ($app) {
    consultarEmpleadosC();
});

$app->get('/consultar/empleado/:id', function($id) use ($app) {
    consultarEmpleadoC($id);
});

$app->post('/actualiza/empleado', function() use ($app) {
    $json = $app->request->post('json');
    $datos = json_decode($json, true);
    actualizaEmpleadoC($datos);
});

$app->get('/elimina/empleado/:id', function($id) use ($app) {
    eliminaEmpleadoC($id);
});
#endregion Empleados

#region Movimientos
$app->post('/crear/movimiento', function() use ($app) {
    $json = $app->request->post('json');
    $datos = json_decode($json, true);
    crearMovimientoC($datos);
});

$app->get('/consultar/movimientos', function() use ($app) {
    consultarMovimientosC();
});

$app->get('/consultar/movimiento/:id', function($id) use ($app) {
    consultarMovimientoC($id);
});

$app->post('/actualiza/movimiento', function() use ($app) {
    $json = $app->request->post('json');
    $datos = json_decode($json, true);
    actualizaMovimientoC($datos);
});

$app->get('/elimina/movimiento/:id', function($id) use ($app) {
    eliminaMovimientoC($id);
});

$app->get('/fecha', function() use ($app) {
    fechaMovimientoC();
});

$app->get('/consultar/empleado/mov/:id', function($id) use ($app) {
    consultarEmpleadoMovC($id);
});
#endregion Movimientos

#region Nomina
$app->get('/crea/nomina/mes/:mes/ano/:ano', function($mes, $ano) use ($app) {
    crearNominaC($mes, $ano);
});

$app->get('/crea/pdf/mes/:mes/ano/:ano', function($mes, $ano) use ($app) {
    crearPDFC($mes, $ano);
});
#endregion Nomina

#region BD
function infoServidor() {
    $host = "localhost";
    $dbname = "nomina_rinku";
    $user = "sapienx";
    $password = "12345";
    return new \PDO("sqlsrv:Server=$host;Database=$dbname", $user, $password, array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
    //return new \PDO("dblib:host=$host;dbname=$dbname;charset=utf8", $user, $password, array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
}

// function infoServidor() {
//     $host = "230175-B27-5\SQLEXPRESS";
//     $dbname = "nomina_rinku";
//     $user = "sapx";
//     $password = "12345";
//     return new \PDO("sqlsrv:Server=$host;Database=$dbname", $user, $password, array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
//     //return new \PDO("dblib:host=$host;dbname=$dbname;charset=utf8", $user, $password, array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
// }
#endregion BD
