<?php

#region Empleados
    function crearEmpleadoM($datos) {
        $data = null;
        $db = infoServidor();

        $statement = $db->prepare("EXEC proc_utileriaempleado 1, '', :nombre, :rol, :tipo;");
        $statement->bindParam(":nombre", $datos["nombre"], \PDO::PARAM_STR);
        $statement->bindParam(":rol", $datos["rol"], \PDO::PARAM_INT);
        $statement->bindParam(":tipo", $datos["tipo"], \PDO::PARAM_INT);
        $statement->execute();
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet = new \stdClass();
            $resultSet->clave  = $entry["clave_empleado"];
            $data[] = $resultSet;
            $resultSet = null;
        }

        return $data;
    }

    function consultarEmpleadosM() {
        $data = null;
        $db = infoServidor();

        $statement = $db->prepare("EXEC proc_utileriaempleado 2, '', '', '', '';");
        $statement->execute();
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet = new \stdClass();
            $resultSet->clave       = $entry["clave_empleado"];
            $resultSet->nombre      = $entry["nombre"];
            $resultSet->rol         = $entry["rol"];
            $resultSet->tipo        = $entry["tipo"];
            $data[] = $resultSet;
            $resultSet = null;
        }

        return $data;
    }

    function consultarEmpleadoM($id) {
        $data = null;
        $db = infoServidor();

        $statement = $db->prepare("EXEC proc_utileriaempleado 3, :id, '', '', '';");
        $statement->bindParam(":id", $id, \PDO::PARAM_INT);
        $statement->execute();
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet = new \stdClass();
            $resultSet->clave       = $entry["clave_empleado"];
            $resultSet->nombre      = $entry["nombre"];
            $resultSet->rol         = $entry["clave_rol"];
            $resultSet->tipo        = $entry["clave_tipo"];
            $data[] = $resultSet;
            $resultSet = null;
        }

        return $data;
    }

    function actualizaEmpleadoM($datos) {
        $db = infoServidor();

        $statement = $db->prepare("EXEC proc_utileriaempleado 4, :id, :nombre, :rol, :tipo;");
        $statement->bindParam(":id", $datos["clave"], \PDO::PARAM_STR);
        $statement->bindParam(":nombre", $datos["nombre"], \PDO::PARAM_STR);
        $statement->bindParam(":rol", $datos["rol"], \PDO::PARAM_INT);
        $statement->bindParam(":tipo", $datos["tipo"], \PDO::PARAM_INT);
        $statement->execute();
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet = new \stdClass();
            $resultSet->rowAfectadas  = $entry["rowAfectadas"];
            $data[] = $resultSet;
            $resultSet = null;
        }


        if ($data[0]->rowAfectadas > 0) {
            $data = true;
        } else {
            $data = false;
        }

        return $data;
    }

    function eliminaEmpleadoM($id) {
        $data = null;
        $db = infoServidor();

        $statement = $db->prepare("EXEC proc_utileriaempleado 5, :id, '', '', '';");
        $statement->bindParam(":id", $id, \PDO::PARAM_INT);
        $statement->execute();
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet = new \stdClass();
            $resultSet->rowAfectadas  = $entry["rowAfectadas"];
            $data[] = $resultSet;
            $resultSet = null;
        }

        if ($data[0]->rowAfectadas > 0) {
            $data = true;
        } else {
            $data = false;
        }

        return $data;
    }
#endregion Empleados
