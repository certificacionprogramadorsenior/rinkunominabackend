<?php

#region Nomina
function crearNominaM($mes, $ano) {
    $data = null;
    $db = infoServidor();

    $statement = $db->prepare("EXEC proc_utilerianomina 1, :mes, :ano;");
    $statement->bindParam(":mes", $mes, \PDO::PARAM_INT);
    $statement->bindParam(":ano", $ano, \PDO::PARAM_INT);
    $statement->execute();
    $data = true;

    return $data;
}

function pedirInformacion($mes, $ano) {
    $data = null;
    $db = infoServidor();

    $statement = $db->prepare("EXEC proc_utilerianomina 2, :mes, :ano;");
    $statement->bindParam(":mes", $mes, \PDO::PARAM_INT);
    $statement->bindParam(":ano", $ano, \PDO::PARAM_INT);
    $statement->execute();
    while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
        $resultSet = new \stdClass();
        $resultSet->clave_empleado      = $entry["clave_empleado"];
        $resultSet->nombre              = $entry["nombre"];
        $resultSet->rol                 = $entry["rol"];
        $resultSet->tipo                = $entry["tipo"];
        $resultSet->SueldoBase          = $entry["SueldoBase"];
        $resultSet->SueldoEntrega       = $entry["SueldoEntrega"];
        $resultSet->SueldoBono          = $entry["SueldoBono"];
        $data[] = $resultSet;
        $resultSet = null;
    }

    return $data;
}
#endregion Nomina
