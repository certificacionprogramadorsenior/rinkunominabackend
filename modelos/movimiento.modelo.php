<?php

#region Movimientos
function crearMovimientoM($datos) {
    $data = null;
    $db = infoServidor();

    $statement = $db->prepare("EXEC proc_utileriamovimiento 1, '', :clave_empleado, :clave_rol, :fecha, :horas_trabajadas, :entrega");
    $statement->bindParam(":clave_empleado", $datos["clave"], \PDO::PARAM_INT);
    $statement->bindParam(":clave_rol", $datos["cubrio"], \PDO::PARAM_INT);
    $statement->bindParam(":fecha", $datos["fecha"], \PDO::PARAM_STR);
    $statement->bindParam(":horas_trabajadas", $datos["horasTrabajadas"], \PDO::PARAM_INT);
    $statement->bindParam(":entrega", $datos["cantEntregas"], \PDO::PARAM_INT);
    $statement->execute();
    while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
        $resultSet = new \stdClass();
        $resultSet->rowAfectadas  = $entry["rowAfectadas"];
        $data[] = $resultSet;
        $resultSet = null;
    }

    if ($data[0]->rowAfectadas > 0) {
        $data = true;
    } else {
        $data = false;
    }

    return $data;
}

function consultarMovimientosM() {
    $data = null;
    $db = infoServidor();

    $statement = $db->prepare("EXEC proc_utileriamovimiento 2, '', '', '', '', '', '';");
    $statement->execute();
    while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
        $resultSet = new \stdClass();
        $resultSet->claveMovimiento     = $entry["clave_movimiento"];
        $resultSet->clave       = $entry["clave_empleado"];
        $date = date_create($entry["fecha"]);
        $resultSet->fecha               = date_format($date, 'd/m/Y');
        $resultSet->nombre              = $entry["nombre"];
        $resultSet->rol                 = $entry["rol"];
        $resultSet->tipo                = $entry["tipo"];
        $data[] = $resultSet;
        $resultSet = null;
    }

    return $data;
}

function consultarMovimientoM($id) {
    $data = null;
    $db = infoServidor();

    $statement = $db->prepare("EXEC proc_utileriamovimiento 3, :id, '', '', '', '', '';");
    $statement->bindParam(":id", $id, \PDO::PARAM_INT);
    $statement->execute();
    while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
        $resultSet = new \stdClass();
        $resultSet->claveMovimiento     = $entry["clave_movimiento"];
        $resultSet->clave               = $entry["clave_empleado"];
        $resultSet->nombre              = $entry["nombre"];
        $resultSet->tipo                = $entry["tipo"];
        $resultSet->rol                 = $entry["rol"];
        $resultSet->fecha               = $entry["fecha"];
        $resultSet->cubrio              = $entry["clave_rol_cubrio"];
        $resultSet->horasTrabajadas     = $entry["horas_trabajadas"];
        $resultSet->cantEntregas        = $entry["entrega"];
        $data[] = $resultSet;
        $resultSet = null;
    }

    return $data;
}

function actualizaMovimientoM($datos) {
    $db = infoServidor();

    $statement = $db->prepare("EXEC proc_utileriamovimiento 4, :clave_movimiento, :clave_empleado, :clave_rol, :fecha, :horas_trabajadas, :entrega");
    $statement->bindParam(":clave_movimiento", $datos["claveMovimiento"], \PDO::PARAM_INT);
    $statement->bindParam(":clave_empleado", $datos["clave"], \PDO::PARAM_INT);
    $statement->bindParam(":clave_rol", $datos["cubrio"], \PDO::PARAM_INT);
    $statement->bindParam(":fecha", $datos["fecha"], \PDO::PARAM_INT);
    $statement->bindParam(":horas_trabajadas", $datos["horasTrabajadas"], \PDO::PARAM_INT);
    $statement->bindParam(":entrega", $datos["cantEntregas"], \PDO::PARAM_INT);
    $statement->execute();
    while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
        $resultSet = new \stdClass();
        $resultSet->rowAfectadas  = $entry["rowAfectadas"];
        $data[] = $resultSet;
        $resultSet = null;
    }

    if ($data[0]->rowAfectadas > 0) {
        $data = true;
    } else {
        $data = false;
    }

    return $data;
}

function eliminaMovimientoM($id) {
    $data = null;
    $db = infoServidor();

    $statement = $db->prepare("EXEC proc_utileriamovimiento 5, :id, '', '', '', '', '';");
    $statement->bindParam(":id", $id, \PDO::PARAM_INT);
    $statement->execute();
    while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
        $resultSet = new \stdClass();
        $resultSet->rowAfectadas  = $entry["rowAfectadas"];
        $data[] = $resultSet;
        $resultSet = null;
    }

    if ($data[0]->rowAfectadas > 0) {
        $data = true;
    } else {
        $data = false;
    }

    return $data;
}

function fechaMovimientoM() {
    $data = null;
    $db = infoServidor();

    $statement = $db->prepare("EXEC proc_utileriamovimiento 6, '', '', '', '', '', '';");
    $statement->execute();
    while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
        $resultSet = new \stdClass();
        $resultSet->fecha  = $entry["fecha"];
        $data[] = $resultSet;
        $resultSet = null;
    }

    return $data;
}

function consultarEmpleadoMovM($id) {
    $data = null;
    $db = infoServidor();

    $statement = $db->prepare("EXEC proc_utileriamovimiento 7, '', :id, '', '', '', '';");
    $statement->bindParam(":id", $id, \PDO::PARAM_INT);
    $statement->execute();
    while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
        $resultSet = new \stdClass();
        $resultSet->clave       = $entry["clave_empleado"];
        $resultSet->nombre      = $entry["nombre"];
        $resultSet->rol         = $entry["rol"];
        $resultSet->tipo        = $entry["tipo"];
        $data[] = $resultSet;
        $resultSet = null;
    }

    return $data;
}
#endregion Movimientos
